package com.swiggy.maxwell.exception;

public class MaxwellProcessException extends RuntimeException {

  public MaxwellProcessException() {
    super();
  }

  public MaxwellProcessException(String message) {
    super(message);
  }

  public MaxwellProcessException(String message, Throwable t) {
    super(message, t);
  }

  public MaxwellProcessException(Throwable t) {
    super(t);
  }

  protected static String format(String message, Object... args) {
    return String.format(String.valueOf(message), (Object[]) args);
  }
}
