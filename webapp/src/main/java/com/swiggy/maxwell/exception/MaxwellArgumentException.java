package com.swiggy.maxwell.exception;

public class MaxwellArgumentException extends RuntimeException {

  public MaxwellArgumentException() {
    super();
  }

  public MaxwellArgumentException(String message) {
    super(message);
  }

  public MaxwellArgumentException(String message, Throwable t) {
    super(message, t);
  }

  public MaxwellArgumentException(Throwable t) {
    super(t);
  }

  protected static String format(String message, Object... args) {
    return String.format(String.valueOf(message), (Object[]) args);
  }
}
