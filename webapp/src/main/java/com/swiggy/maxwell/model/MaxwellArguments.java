package com.swiggy.maxwell.model;

import lombok.Data;
import lombok.Setter;

@Data
@Setter
public class MaxwellArguments {
  String user;
  String password;
  String host;

  int serverId;
  String replication_user;
  String replication_password;
  String replication_host;

  String database;
  String table;
  String kafkaServer;
  String identifier;
}
