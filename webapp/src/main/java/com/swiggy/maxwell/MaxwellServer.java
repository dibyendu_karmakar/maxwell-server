package com.swiggy.maxwell;

import com.djdch.log4j.StaticShutdownCallbackRegistry;
import com.swiggy.maxwell.service.FailoverService;
import com.swiggy.maxwell.service.MaxwellProcessService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class MaxwellServer {
	private static final Logger LOG = LoggerFactory.getLogger(MaxwellServer.class);

	@Autowired
	private MaxwellProcessService maxwellProcessService;
	@Autowired
	private FailoverService failover;

	public static void main(String[] args) {
		SpringApplication.run(MaxwellServer.class, args);
	}

	@PostConstruct
	public void init() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				LOG.info("Calling shutdown hook...");
				System.out.println("Calling shoutdown hook...");
				stopProcess();
				StaticShutdownCallbackRegistry.invoke();
			}
		});
		failover.start();
	}

	public void stopProcess() {
		failover.terminate();
		maxwellProcessService.terminateAll();
	}
}
