package com.swiggy.maxwell.controller;

import com.swiggy.maxwell.service.MaxwellProcessService;
import com.swiggy.maxwell.exception.MaxwellArgumentException;
import com.swiggy.maxwell.model.MaxwellArguments;
import com.swiggy.maxwell.utils.State;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class MaxwellController {
  private static final Logger LOG = LoggerFactory.getLogger(MaxwellController.class);

  @Autowired
  private MaxwellProcessService service;

  @RequestMapping(value = "/config/{name}", method = RequestMethod.GET)
  public MaxwellArguments getConfiguration(@PathVariable String name) {
    MaxwellArguments arguments = service.getConfiguration(name);
    return arguments;
  }

  @RequestMapping(value = "/config", method = RequestMethod.GET)
  public Map<String, MaxwellArguments> getConfiguration() {
    Map<String, MaxwellArguments> configs = service.getConfigurationPool();
    return configs;
  }

  @RequestMapping(value = "/status/{table}", method = RequestMethod.GET)
  public ResponseEntity getStatus(@PathVariable String table) {
    LOG.info("Fetching status for table {}", table);
    State status = service.getStatus(table);
    if (status.equals(State.UNKNOWN)) {
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Table not found");
    }
    return ResponseEntity.status(HttpStatus.FOUND).body(status);
  }

  @RequestMapping(value = "/status", method = RequestMethod.GET)
  public Map<String, State> getAllStatus() {
    LOG.info("Fetching status for all running tasks.");
    Map<String, State> allStatus = service.getAllStatus();
    return allStatus;
  }

  @RequestMapping(value = "/restart/{name}", method = RequestMethod.PUT)
  public ResponseEntity restart(@PathVariable String name) {
    LOG.info("Restarting maxwell for {}", name);
    boolean status = service.restart(name);
    int statusCode = status ? 200 : 500;
    return ResponseEntity.status(statusCode).build();
  }

  @RequestMapping(value = "/start/table", method = RequestMethod.POST)
  public ResponseEntity start(@RequestBody MaxwellArguments args) {
    // TODO table arg check
    service.checkIfExists(args.getTable());
    LOG.info("Starting maxwell for table {}", args.getTable());
    LOG.info("with arguments:");
    LOG.info(args.toString());
    try {
      boolean status = service.start(args);
      return status ?
          ResponseEntity.status(HttpStatus.ACCEPTED).body("Successfully started.") :
          ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Check arguments and db connections");
    } catch (MaxwellArgumentException me) {
      return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(me.getMessage());
    }
  }

  @RequestMapping(value = "/start/db", method = RequestMethod.POST)
  public ResponseEntity start_db(MaxwellArguments args) {
    service.checkIfExists(args.getDatabase());
    LOG.info("Starting maxwell for table {}", args.getTable());
    LOG.info("with arguments:");
    LOG.info(args.toString());
    try {
      boolean status = service.startForDB(args);
      return status ?
          ResponseEntity.status(HttpStatus.ACCEPTED).body("Successfully started.") :
          ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Check arguments and db connections");
    } catch (MaxwellArgumentException me) {
      return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(me.getMessage());
    }
  }

  @RequestMapping(value = "/stop", method = RequestMethod.PUT)
  public ResponseEntity stopAll() {
    LOG.info("Stopping maxwell for all the tables");
    service.terminateAll();
    return service.isAllTerminated() ?
        ResponseEntity.status(HttpStatus.OK).body("Terminated all the processes") :
        ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Unable to terminate");
  }

  @RequestMapping(value = "/stop/{name}", method = RequestMethod.PUT)
  public ResponseEntity stop(@PathVariable String name) {
    LOG.info("Stopping maxwell for table {}", name);
    boolean status = service.stopListener(name);
    return status ?
        ResponseEntity.status(HttpStatus.ACCEPTED).body("Terminated.") :
        ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Unable to Terminate.");
  }
}
