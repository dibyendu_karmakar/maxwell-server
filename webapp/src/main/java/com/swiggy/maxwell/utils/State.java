package com.swiggy.maxwell.utils;

public enum State { STARTING, STOPPED, RUNNING, REQUEST_STOP, TERMINATED, UNKNOWN }