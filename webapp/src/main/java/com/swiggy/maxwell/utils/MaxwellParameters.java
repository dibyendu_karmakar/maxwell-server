package com.swiggy.maxwell.utils;

import lombok.Data;

@Data
public class MaxwellParameters {
  public static String user = "--user";
  public static String password = "--password";
  public static String host = "--host";

  public static String client_id = "--client_id";
  public static String replica_server_id = "--replica_server_id";
  public static String replication_user = "--replication_user";
  public static String replication_password = "--replication_password";
  public static String replication_host = "--replication_host";

  public static String producer = "--producer";
  public static String kafkaServer = "--kafka.bootstrap.servers";
  public static String kafkaTopic = "--kafka_topic";
  public static String filter = "--filter";

  public static String ignoreProducerError = "--ignore_producer_error";

  public static String kafkaTopicPattern = "maxwell_%s_%s";
  public static String tableFilterPattern = "exclude: *.*, include: %s.%s";
  public static String dbFilterPattern = "exclude: *.*, include: %s.*";
  public static String clientIdPattern = "maxwell_%s";

}