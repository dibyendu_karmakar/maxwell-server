package com.swiggy.maxwell.service;

import com.swiggy.maxwell.exception.MaxwellArgumentException;
import com.swiggy.maxwell.model.MaxwellArguments;
import com.swiggy.maxwell.utils.MaxwellParameters;
import com.swiggy.maxwell.utils.State;
import com.zendesk.maxwell.Maxwell;
import com.zendesk.maxwell.MaxwellConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URISyntaxException;
import java.sql.SQLException;

public class MaxwellProcess implements Runnable {
  private static final Logger LOG = LoggerFactory.getLogger(MaxwellProcess.class);

  private State state;
  private final MaxwellArguments arguments;
  private final String name;
  private final boolean isDB;
  private Maxwell maxwell;
  private Thread maxwellThread;

  public MaxwellProcess(MaxwellArguments arguments) {
    // Set initial state as starting.
    state = State.STARTING;
    this.arguments = arguments;
    this.maxwell = getMaxwell(this.arguments);
    this.name = arguments.getTable();
    this.isDB = false;
    setMaxwellThread();
  }

  protected void setMaxwellThread() {
    this.maxwellThread = new Thread(this);
  }

  protected void setMaxwell() {
    this.state = State.STARTING;
    if(isDB) {
      this.maxwell = getMaxwellForDB(this.arguments);
    } else {
      this.maxwell = getMaxwell(arguments);
    }
  }

  public MaxwellProcess(MaxwellArguments arguments, boolean db) {
    // Set initial state as starting.
    state = State.STARTING;
    this.arguments = arguments;
    this.name = arguments.getDatabase();
    this.isDB = db;
    if (isDB) {
      this.maxwell = getMaxwellForDB(arguments);
    } else {
      this.maxwell = getMaxwell(arguments);
    }
    setMaxwellThread();
  }

  private Maxwell getMaxwellForDB(MaxwellArguments arguments) {
    String[] args = createMaxwellArgumentsForDB(arguments);
    MaxwellConfig config = new MaxwellProcessConfig(args);
    //config.validate();
    Maxwell maxwellConnector = null;
    try {
      LOG.info("Creating maxwell object...");
      maxwellConnector = new Maxwell(config);
    } catch (SQLException e) {
      LOG.error("Unable to start maxwell for database {}", arguments.getDatabase(), e);
      throw new MaxwellArgumentException(e.getMessage());
    } catch (URISyntaxException e) {
      LOG.error("Unable to start maxwell for database {}", arguments.getDatabase(), e);
      throw new MaxwellArgumentException(e.getMessage());
    }

    return maxwellConnector;
  }

  private Maxwell getMaxwell(MaxwellArguments arguments) {
    String[] args = createMaxwellArguments(arguments);
    MaxwellConfig config = new MaxwellConfig(args);
    Maxwell maxwellConnector = null;
    try {
      LOG.info("Creating maxwell object...");
      maxwellConnector = new Maxwell(config);
    } catch (SQLException e) {
      LOG.error("Unable to start maxwell for table {}", arguments.getTable(), e);
    } catch (URISyntaxException e) {
      LOG.error("Unable to start maxwell for table {}", arguments.getTable(), e);
    }

    return maxwellConnector;
  }

  @Override
  public void run() {
    LOG.info("Starting maxwell for {}", name);
    if (maxwell != null) {
      state = State.RUNNING;
      maxwell.run();
    }
    if(!state.equals(State.REQUEST_STOP)) {
      state = State.STOPPED;
    }
  }

  public void start() {
    maxwellThread.start();
  }

  public void stop() {
    state = State.REQUEST_STOP;
    maxwell.terminate();
    state = State.TERMINATED;
  }

  public State getState() {
    return state;
  }

  private static String[] createMaxwellArguments(MaxwellArguments arguments) {
    String user = MaxwellParameters.user.concat("=").concat(arguments.getUser());
    String password = MaxwellParameters.password.concat("=").concat(arguments.getPassword());
    String host = MaxwellParameters.host.concat("=").concat(arguments.getHost());

    String client_id_val = String.format(MaxwellParameters.clientIdPattern, arguments.getIdentifier());
    String client_id = MaxwellParameters.client_id.concat("=").concat(client_id_val);
    String server_id = MaxwellParameters.replica_server_id.concat("=") + (arguments.getServerId());
    String replication_host = MaxwellParameters.replication_host.concat("=").concat(arguments.getReplication_host());
    String replication_password =
        MaxwellParameters.replication_password.concat("=").concat(arguments.getReplication_password());
    String replication_user = MaxwellParameters.replication_user.concat("=").concat(arguments.getReplication_user());

    String tableFilter =
        String.format(MaxwellParameters.tableFilterPattern, arguments.getDatabase(), arguments.getTable());
    String filter = MaxwellParameters.filter.concat("=").concat(tableFilter);

    String procuder = MaxwellParameters.producer.concat("=").concat("kafka");
    String kafkaServer = MaxwellParameters.kafkaServer.concat("=").concat(arguments.getKafkaServer());
    String topicName =
        String.format(MaxwellParameters.kafkaTopicPattern, arguments.getIdentifier(), arguments.getTable());
    String kafkaTopic = MaxwellParameters.kafkaTopic.concat("=").concat(topicName);

    String ignoreError = MaxwellParameters.ignoreProducerError.concat("=").concat("false");

    return new String[] { user, password, host, client_id, server_id, replication_host, replication_password, replication_user,
        filter, procuder, kafkaServer, kafkaTopic, ignoreError };
  }

  private static String[] createMaxwellArgumentsForDB(MaxwellArguments arguments) {
    String user = MaxwellParameters.user.concat("=").concat(arguments.getUser());
    String password = MaxwellParameters.password.concat("=").concat(arguments.getPassword());
    String host = MaxwellParameters.host.concat("=").concat(arguments.getHost());

    String client_id_val = String.format(MaxwellParameters.clientIdPattern, arguments.getIdentifier());
    String client_id = MaxwellParameters.client_id.concat("=").concat(client_id_val);
    String server_id = MaxwellParameters.replica_server_id.concat("=") + (arguments.getServerId());
    String replication_host = MaxwellParameters.replication_host.concat("=").concat(arguments.getReplication_host());
    String replication_password =
        MaxwellParameters.replication_password.concat("=").concat(arguments.getReplication_password());
    String replication_user = MaxwellParameters.replication_user.concat("=").concat(arguments.getReplication_user());

    String tableFilter = String.format(MaxwellParameters.tableFilterPattern, arguments.getDatabase(), '*');
    String filter = MaxwellParameters.filter.concat("=").concat(tableFilter);

    String producer = MaxwellParameters.producer.concat("=").concat("kafka");
    String kafkaServer = MaxwellParameters.kafkaServer.concat("=").concat(arguments.getKafkaServer());
    String topicName = String.format(MaxwellParameters.kafkaTopicPattern, arguments.getIdentifier(), "%{table}");
    String kafkaTopic = MaxwellParameters.kafkaTopic.concat("=").concat(topicName);

    String ignoreError = MaxwellParameters.ignoreProducerError.concat("=").concat("false");

    return new String[] { user, password, host, client_id, server_id, replication_host, replication_password, replication_user,
        filter, producer, kafkaServer, kafkaTopic, ignoreError };
  }
}
