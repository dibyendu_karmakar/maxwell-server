package com.swiggy.maxwell.service;

import com.swiggy.maxwell.model.MaxwellArguments;
import com.swiggy.maxwell.utils.State;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Set;

/**
 * This service periodically checks maxwell executor pool for stopped thread.
 * If any thread is stopped it will start that thread.
 */
@Service
public class FailoverService implements Runnable {
  private static final Logger LOG = LoggerFactory.getLogger(FailoverService.class);

  private MaxwellProcessService maxwellService;
  Map<String, MaxwellArguments> configurationPool;
  Map<String, MaxwellProcess> executorPool;

  private boolean terminate = false;

  Thread failover;

  @Autowired
  public FailoverService(MaxwellProcessService maxwellController) {
    this.maxwellService = maxwellController;
    this.configurationPool = maxwellController.getConfigurationPool();
    this.executorPool = maxwellController.getExecutorPool();
    failover = new Thread(this);
  }

  public void start() {
    LOG.info("Starting failover service.");
    failover.start();
  }

  @Override
  public void run() {
    while (!terminate) {
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        LOG.error(e.getMessage());
      }
      Set<String> maxprocs = executorPool.keySet();
      for (String maxproc : maxprocs) {
        MaxwellProcess maxProc = executorPool.get(maxproc);
        if (maxProc.getState().equals(State.STOPPED)) {
          LOG.info("Maxwell process for {} was stopped. Restarting...", maxproc);
          maxwellService.startNewInstance(maxProc);
        }
      }
    }
  }

  public void terminate() {
    terminate = true;
  }
}
