package com.swiggy.maxwell.service;

import com.swiggy.maxwell.exception.MaxwellProcessException;
import com.swiggy.maxwell.model.MaxwellArguments;
import com.swiggy.maxwell.utils.State;
import com.zendesk.maxwell.Maxwell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * {@link MaxwellProcessService} is responsible for managing maxwell processes e.g. start, stop, restart, getstatus.
 */
@Service
public class MaxwellProcessService {
  private static final Logger LOG = LoggerFactory.getLogger(MaxwellProcessService.class);

  /** Map<table_name, maxwell_process> Contains Maxwell processes */
  private Map<String, MaxwellProcess> executorPool;
  /** Map<table_name, config> Contains Maxwell arguments for the table */
  private Map<String, MaxwellArguments> configurationPool;

  private static int server_id = new Random().nextInt(100);

  ReentrantReadWriteLock reentrantReadWriteLock;
  private final ReentrantReadWriteLock.ReadLock readLock;
  private final ReentrantReadWriteLock.WriteLock writeLock;

  public MaxwellProcessService() {
    configurationPool = new HashMap<>();
    executorPool = new HashMap<>();
    reentrantReadWriteLock = new ReentrantReadWriteLock();
    readLock = reentrantReadWriteLock.readLock();
    writeLock = reentrantReadWriteLock.writeLock();
  }

  /**
   * Start Maxwell for all the table in the db
   *
   * @param arguments
   * @return
   */
  public boolean startForDB(MaxwellArguments arguments) {
    LOG.info("Starting maxwell service for database {}", arguments.getDatabase());
    arguments.setServerId(++server_id);
    configurationPool.put(arguments.getDatabase(), arguments);
    MaxwellProcess maxProc = startMaxwellForDB(arguments);
    if (maxProc.getState().equals(State.STOPPED)) {
      LOG.error("Unable to start maxwell. Check the arguments and database connections.");
      return false;
    }

    LOG.info("Successfully started maxwell for database {}", arguments.getDatabase());
    executorPool.put(arguments.getDatabase(), maxProc);
    return true;
  }

  private MaxwellProcess startMaxwellForDB(MaxwellArguments arguments) {
    LOG.debug("Creating MaxwellProcess object.");
    MaxwellProcess maxwell = new MaxwellProcess(arguments, true);
    maxwell.start();
    return maxwell;
  }

  /**
   * Start Maxwell for a specific table.
   *
   * @param arguments
   * @return
   */
  public boolean start(MaxwellArguments arguments) {
    LOG.info("Starting maxwell service for table {}", arguments.getTable());
    arguments.setServerId(++server_id);
    configurationPool.put(arguments.getTable(), arguments);
    MaxwellProcess maxProc = new MaxwellProcess(arguments);
    startMaxwell(maxProc);

    if (maxProc.getState().equals(State.STOPPED)) {
      LOG.error("Unable to start maxwell. Check the arguments and database connections.");
      return false;
    }

    LOG.info("Successfully started maxwell for table {}", arguments.getTable());
    executorPool.put(arguments.getTable(), maxProc);
    return true;
  }

  public void startMaxwell(MaxwellProcess maxwell) {
    LOG.debug("Creating MaxwellProcess object.");
    maxwell.start();
  }

  public void checkIfExists(String name) {
    if (configurationPool.containsKey(name)) {
      State state = executorPool.get(name).getState();
      throw new MaxwellProcessException("Already exists in " + state.name() + " status.");
    }
  }

  public boolean restart(String maxproc) {
    LOG.info("Restarting maxwell process for {}.", maxproc);
    MaxwellProcess maxProc = executorPool.get(maxproc);
    if (maxProc.getState().equals(State.RUNNING)) {
      LOG.debug("Stopping maxwell process for {}", maxProc);
      maxProc.stop();
    }
    if (maxProc.getState().equals(State.STOPPED) || maxProc.getState().equals(State.TERMINATED)) {
      LOG.debug("Starting maxwell process for {}", maxProc);
      startNewInstance(maxProc);
    } else {
      LOG.info("Unable to restart maxwell for {}.", maxProc);
      return false;
    }

    if (maxProc.getState().equals(State.STOPPED) || maxProc.getState().equals(State.TERMINATED)) {
      return false;
    }
    return true;
  }

  protected void startNewInstance(MaxwellProcess maxProc) {
    maxProc.setMaxwell();
    maxProc.setMaxwellThread();
    maxProc.start();
  }

  public boolean stopListener(String maxproc) {
    LOG.info("Stopping maxwell process for {}", maxproc);
    MaxwellProcess maxwell = executorPool.get(maxproc);
    maxwell.stop();
    //removeFromPool(table);
    if (maxwell.getState().equals(State.TERMINATED)) {
      return true;
    }
    return false;
  }

  public void terminateAll() {
    LOG.info("Terminating all maxwell process.");
    Set<String> tables = executorPool.keySet();
    LOG.debug("Terminating {}", String.join(",", tables));
    for (String table : tables) {
      MaxwellProcess maxwell = executorPool.get(table);
      maxwell.stop();
      //removeFromPool(table);
    }
  }

  public Map<String, MaxwellProcess> getExecutorPool() {
    return executorPool;
  }

  public Map<String, MaxwellArguments> getConfigurationPool() {
    return configurationPool;
  }

  public void updatePool(String table, MaxwellProcess newProc) {
    writeLock.lock();
    executorPool.put(table, newProc);
    writeLock.unlock();
  }

  public void removeFromPool(String table) {
    if (executorPool.get(table).getState().equals(State.TERMINATED)) {
      writeLock.lock();
      executorPool.remove(table);
      writeLock.unlock();
    }
  }

  public State getStatus(String table) {
    MaxwellProcess maxProc = executorPool.get(table);
    return maxProc != null ? maxProc.getState() : State.UNKNOWN;
  }

  public Map<String, State> getAllStatus() {
    Map<String, State> all_status = new HashMap<>();
    for (Map.Entry<String, MaxwellProcess> entry : executorPool.entrySet()) {
      all_status.put(entry.getKey(), entry.getValue().getState());
    }
    return all_status;
  }

  public boolean isAllTerminated() {
    for (Map.Entry<String, MaxwellProcess> entry : executorPool.entrySet()) {
      if (!entry.getValue().getState().equals(State.TERMINATED)) {
        return false;
      }
    }
    return true;
  }

  public MaxwellArguments getConfiguration(String name) {
    return configurationPool.get(name);
  }
}
