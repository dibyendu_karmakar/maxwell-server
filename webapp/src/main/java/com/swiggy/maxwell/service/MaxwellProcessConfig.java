package com.swiggy.maxwell.service;

import com.swiggy.maxwell.exception.MaxwellArgumentException;
import com.zendesk.maxwell.MaxwellConfig;
import joptsimple.BuiltinHelpFormatter;
import joptsimple.OptionDescriptor;
import joptsimple.OptionParser;

import java.io.IOException;
import java.util.Map;

public class MaxwellProcessConfig extends MaxwellConfig {

  public MaxwellProcessConfig(String[] argv) {
    super(argv);
  }

  @Override
  protected void usageForOptions(String string, final String... filterOptions) {
    BuiltinHelpFormatter filteredHelpFormatter = new BuiltinHelpFormatter(200, 4) {
      public String format(Map<String, ? extends OptionDescriptor> options) {
        this.addRows(options.values());
        String output = this.formattedHelpOutput();
        String[] lines = output.split("\n");
        String filtered = "";
        int i = 0;
        String[] var6 = lines;
        int var7 = lines.length;

        for (int var8 = 0; var8 < var7; ++var8) {
          String l = var6[var8];
          boolean showLine = false;
          if (l.contains("--help") || i++ < 2) {
            showLine = true;
          }

          String[] var11 = filterOptions;
          int var12 = var11.length;

          for (int var13 = 0; var13 < var12; ++var13) {
            String o = var11[var13];
            if (l.contains(o)) {
              showLine = true;
            }
          }

          if (showLine) {
            filtered = filtered + l + "\n";
          }
        }

        return filtered;
      }
    };
    System.err.println(string);
    System.err.println();
    OptionParser p = this.buildOptionParser();
    p.formatHelpWith(filteredHelpFormatter);

    try {
      p.printHelpOn(System.err);
      throw new MaxwellArgumentException(string);
    } catch (IOException var6) {
    }

  }
}
